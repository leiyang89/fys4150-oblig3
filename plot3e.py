from scitools.std import *
import sys

f = open('3e1000.dat')
x1 = zeros(10000);
y1 = zeros(10000);
x2 = zeros(10000);
y2 = zeros(10000);
x3 = zeros(10000);
y3 = zeros(10000);
for i in range(10000):
	x1[i] = float(f.readline())
	y1[i] = float(f.readline())
	x2[i] = float(f.readline())
	y2[i] = float(f.readline())	
f.close()
g = open('3bdata.dat')
for i in range(10000):
	x3[i] = float(g.readline())
	y3[i] = float(g.readline())
g.close()
#plot(x1,y1, 'ro')
#hold('on')
plot(x2,y2, 'bo')
hold('on')
plot(x3,y3, 'gx')
xlabel('x')
ylabel('y')
legend('Earth', 'Earth Unperturbed')
title('Trajectories of Earth and non-perturbed Earth')
savefig('JupiterAndEarthSuperposed1000.png')
