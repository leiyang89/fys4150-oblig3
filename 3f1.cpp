/*
3 body problem. All objects orbit around the center of mass
*/

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

//global constants
double PI = atan(1.0f) * 4.0f; // the value of pi 

//create class for planets and star. Body short for planetary body...
class Body {
	public:
	//state. m refers to relative mass wrt the sun, i.e. relative force
	double x, y, u, v, m;
	double dx, dy, du, dv;
	void init(double, double, double, double, double);
	void set(int, double);
	void setd(int, double);
	double val(int);
	double dval(int);
	void move(Body&);
	void superpose(Body&);
	double mass();
};
void Body::init(double a, double b, double c, double d, double e) {
	x = a;
	y = b;
	u = c;
	v = d;
	m = e;
	dx = 0.;
	dy = 0.;
	du = 0.;
	dv = 0.;
}
double Body::mass() {
	return m;
}
void Body::set(int i, double a) {
	switch (i) {
		case 0:
			x = a;
			break;
		case 1:
			y = a;
			break;
		case 2:
			u = a;
			break;
		case 3:
			v = a;
			break;
	}
}

void Body::setd(int i, double a) {
	switch (i) {
		case 0:
			dx = a;
			break;
		case 1:
			dy = a;
			break;
		case 2:
			du = a;
			break;
		case 3:
			dv = a;
			break;
	}
}

double Body::val(int i) {
	switch(i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return u;
		case 3:
			return v;
	}
}
double Body::dval(int i) {
	switch(i) {
		case 0:
			return dx;
		case 1:
			return dy;
		case 2:
			return du;
		case 3:
			return dv;
	}	
}

//Body is moved by the force of first planet
void Body::move(Body &ext) {
	dx = u;
	dy = v;
	du = -ext.mass()*4.*pow(PI,2)*(x-ext.val(0))/pow(pow(x-ext.val(0),2)+pow(y-ext.val(1),2),(3./2));
	dv = -ext.mass()*4.*pow(PI,2)*(y-ext.val(1))/pow(pow(x-ext.val(0),2)+pow(y-ext.val(1),2),(3./2));
}

//Force from subsequent planets superposed
void Body::superpose(Body &external) {
	du += -external.mass()*4.*pow(PI,2)*(x-external.val(0))/pow(pow(x-external.val(0),2)+pow(y-external.val(1),2),(3./2));
	dv += -external.mass()*4.*pow(PI,2)*(y-external.val(1))/pow(pow(x-external.val(0),2)+pow(y-external.val(1),2),(3./2));
}


//global functions below


//initialise function
void initialize (Body &Sun, Body &Jupiter, Body &Earth) {
	Sun.init(-0.0052,0,0,-0.003, 1);
	Jupiter.init(5.19,0,0,2.752, pow(10.,-3));
	Earth.init(0.995,0,0,6.28, 3.3*pow(10,-5));
}

void derivatives(Body &P, Body &ext1, Body &ext2, double *k, double *dy) {
	//P is the object we are working on
	// ext1 and ext2 are the other bodies exerting force on planet
	dy[0] = P.val(2) + k[2];
	dy[1] = P.val(3) + k[3];
	dy[2] = -4.*ext1.mass()*pow(PI,2)*(P.val(0) - ext1.val(0) + k[0])/pow(pow(P.val(0) - ext1.val(0) + k[0],2) + pow(P.val(1) - ext1.val(1) + k[1],2),(3./2))
			-4.*ext2.mass()*pow(PI,2)*(P.val(0) - ext2.val(0) + k[0])/pow(pow(P.val(0) - ext2.val(0) + k[0],2) + pow(P.val(1) - ext2.val(1) + k[1],2),(3./2));
	dy[3] = -4.*ext1.mass()*pow(PI,2)*(P.val(1) - ext1.val(1) + k[0])/pow(pow(P.val(0) - ext1.val(0) + k[0],2) + pow(P.val(1) - ext1.val(1) + k[1],2),(3./2))
			-4.*ext2.mass()*pow(PI,2)*(P.val(1) - ext2.val(1) + k[0])/pow(pow(P.val(0) - ext2.val(0) + k[0],2) + pow(P.val(1) - ext2.val(1) + k[1],2),(3./2));
}

//Runge-Kutta function
void runge_kutta_4(Body &Planet, Body &ext1, Body &ext2, double h, void (*derivs)(Body &, Body &, Body&, double*, double *))
{
  int i; double newstate;
  double h2,h6; 
  double *k1, *k2, *k3, *k4, *f;
  //   allocate space for local vectors 
  k1 = new double [4];
  k2 =  new double [4];
  k3 =  new double [4];
  k4 = new double [4];
  f = new double [4]; //stores temporary f(y+k1/2) etc
  h6 = h/6.;
  h2 = .5*h;
  //get dy and find k1
  for (i = 0; i < 4; i++) { // k1
	//k1 is h*k1/2 for easier use of derivs
	k1[i] = h2*Planet.dval(i);
  }
  (*derivs)(Planet, ext1, ext2, k1, f);     // k2   
  for (i = 0; i < 4; i++) {//k2
    k2[i] = h2*f[i];//k2 is k2/2 for easier use of derivs
  }
  (*derivs)(Planet, ext1, ext2, k2, f); //  k3 
  for (i=0; i < 4; i++) {//k3
    k3[i] = h*f[i];//k3 is normal 
  }
  (*derivs)(Planet, ext1, ext2, k3, f);    // k4 
  for (i=0; i < 4; i++) {//k4
	k4[i] = h*f[i];  //k4 is normal
  }
  //      now we upgrade state of Body  
  for (i = 0; i < 4; i++){
	newstate = Planet.val(i)+1./6.*(2.*k1[i]+4.*k2[i]+2.*k3[i] + 1.*k4[i]);
    Planet.set(i,newstate);
  }
  delete []k1;
  delete [] k2;
  delete [] k3;
  delete [] k4;
  delete [] f;
} 


//main below
int main(int argc, char* argv[]) {

	//declare variables
	Body Earth;
	Body Jupiter;
	Body Sun;
	double t, h, tmax, E; 
	int steps;

	//open output file
	ofstream outfile;
 	outfile.open("3body.dat");
	
	//let user define timescale
	cout << "Number of steps? " << endl;
	cin >> steps;
	cout << "Total time? " << endl;
	cin >> tmax;
	initialize(Sun, Jupiter, Earth);
	h = 1.*tmax/steps;
	t = 0.;

	// loop calculations until assigned time-range has been reached
    while (t <= tmax){
		 //find initial derivatives due to all states
		Sun.move(Jupiter);
		Sun.superpose(Earth);
   		Jupiter.move(Sun);
		Jupiter.superpose(Earth);    
		Earth.move(Sun);
		Earth.superpose(Jupiter);

		//perform rk4 one step, increment one timestep, and save state to outfile  
		runge_kutta_4(Sun, Jupiter, Earth, h, derivatives);	     
   		runge_kutta_4(Jupiter, Sun, Earth, h, derivatives); 	
		runge_kutta_4(Earth, Jupiter, Sun, h, derivatives);	
		
		E = .5*(pow(Earth.val(2),2) + pow(Earth.val(3),2)) 
			- 4.*pow(PI,2)/(pow(pow(Earth.val(0),2) + pow(Earth.val(1),2),1./2))
			+ .5*(pow(Jupiter.val(2),2) + pow(Jupiter.val(3),2)) 
			- 4.*pow(PI,2)/(pow(pow(Jupiter.val(0),2) + pow(Jupiter.val(1),2),1./2))
			+ .5*(pow(Sun.val(2),2) + pow(Sun.val(3),2)) 
			- 4.*pow(PI,2)/(pow(pow(Sun.val(0),2) + pow(Sun.val(1),2),1./2));
		
    	t += h;
		outfile << Earth.val(0) << endl;
		outfile << Earth.val(1) << endl;	
		outfile << Jupiter.val(0) << endl;
		outfile << Jupiter.val(1) << endl;
		outfile << Sun.val(0) << endl;
		outfile << Sun.val(1) << endl;
		//outfile << t << endl;
		//outfile << E << endl;
  	}
    outfile.close();  // close output file

	return 0;
}


























