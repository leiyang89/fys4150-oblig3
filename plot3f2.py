from scitools.std import *
import sys

f = open('9body.dat')
x1 = zeros(1000);
y1 = zeros(1000);
x2 = zeros(1000);
y2 = zeros(1000);
x3 = zeros(1000);
y3 = zeros(1000);
x4 = zeros(1000);
y4 = zeros(1000);
x5 = zeros(1000);
y5 = zeros(1000);
x6 = zeros(1000);
y6 = zeros(1000);
x7 = zeros(1000);
y7 = zeros(1000);
x8 = zeros(1000);
y8 = zeros(1000);
x9 = zeros(1000);
y9 = zeros(1000);
for i in range(1000):
	x1[i] = float(f.readline())
	y1[i] = float(f.readline())
	x2[i] = float(f.readline())
	y2[i] = float(f.readline())	
	x3[i] = float(f.readline())
	y3[i] = float(f.readline())	
	x4[i] = float(f.readline())
	y4[i] = float(f.readline())
	x5[i] = float(f.readline())
	y5[i] = float(f.readline())	
	x6[i] = float(f.readline())
	y6[i] = float(f.readline())
	x7[i] = float(f.readline())
	y7[i] = float(f.readline())
	x8[i] = float(f.readline())
	y8[i] = float(f.readline())	
	x9[i] = float(f.readline())
	y9[i] = float(f.readline())
f.close()

plot(x1,y1, 'ro')
hold('on')
plot(x2,y2)
plot(x3,y3)
plot(x4,y4)
plot(x5,y5)
plot(x6,y6)
plot(x7,y7)
plot(x8,y8)
plot(x9,y9)
xlabel('x')
ylabel('y')
legend('Sun', 'Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune')
title('Trajectories of 9-body system')
savefig('NineBody.png')
