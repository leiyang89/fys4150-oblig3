/*
9 body problem. All objects orbit around the center of mass
*/

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

//global constants
double PI = atan(1.0f) * 4.0f; // the value of pi 

//create class for planets and star. Body short for planetary body...
class Body {
	public:
	//state. m refers to relative mass wrt the sun, i.e. relative force
	double x, y, u, v, m;
	double dx, dy, du, dv;
	void init(double, double, double, double, double);
	void set(int, double);
	void setd(int, double);
	double val(int);
	double dval(int);
	void move(Body&);
	void superpose(Body&);
	double mass();
};
void Body::init(double a, double b, double c, double d, double e) {
	x = a;
	y = b;
	u = c;
	v = d;
	m = e;
	dx = 0.;
	dy = 0.;
	du = 0.;
	dv = 0.;
}
double Body::mass() {
	return m;
}
void Body::set(int i, double a) {
	switch (i) {
		case 0:
			x = a;
			break;
		case 1:
			y = a;
			break;
		case 2:
			u = a;
			break;
		case 3:
			v = a;
			break;
	}
}

void Body::setd(int i, double a) {
	switch (i) {
		case 0:
			dx = a;
			break;
		case 1:
			dy = a;
			break;
		case 2:
			du = a;
			break;
		case 3:
			dv = a;
			break;
	}
}

double Body::val(int i) {
	switch(i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return u;
		case 3:
			return v;
	}
}
double Body::dval(int i) {
	switch(i) {
		case 0:
			return dx;
		case 1:
			return dy;
		case 2:
			return du;
		case 3:
			return dv;
	}	
}

//Body is moved by the force of first planet
void Body::move(Body &ext) {
	dx = u;
	dy = v;
	du = -ext.mass()*4.*pow(PI,2)*(x-ext.val(0))/pow(pow(x-ext.val(0),2)+pow(y-ext.val(1),2),(3./2));
	dv = -ext.mass()*4.*pow(PI,2)*(y-ext.val(1))/pow(pow(x-ext.val(0),2)+pow(y-ext.val(1),2),(3./2));
}

//Force from subsequent planets superposed
void Body::superpose(Body &external) {
	du += -external.mass()*4.*pow(PI,2)*(x-external.val(0))/pow(pow(x-external.val(0),2)+pow(y-external.val(1),2),(3./2));
	dv += -external.mass()*4.*pow(PI,2)*(y-external.val(1))/pow(pow(x-external.val(0),2)+pow(y-external.val(1),2),(3./2));
}


//global functions below


//initialise function
void initialize (Body &Sun, Body &Mercury, Body &Venus, Body &Earth, Body &Mars, Body &Jupiter, Body &Saturn, Body &Uranus, Body &Neptune) {
	Sun.init(-0.0078,0,0,-0.003, 1);
	Mercury.init(-0.0078+0.39,0,0,-0.003+10.1, 1.65*pow(10,-7));
	Venus.init(-0.0078+0.72,0,0,-0.003+7.39, 2.44*pow(10,-6));
	Earth.init(-0.0078+1,0,0,-0.003+6.28, 2.985*pow(10,-6));
	Mars.init(-0.0078+1.52,0,0,-0.003+5.08, 3.21*pow(10,-7));
	Jupiter.init(-0.0078+5.2,0,0,-0.003+2.76, pow(10,-3));
	Saturn.init(-0.0078+9.58,0,0,-0.003+2.04, 2.84*pow(10,-5));
	Uranus.init(-0.0078+19.23,0,0,-0.003+1.44, 4.34*pow(10,-5));
	Neptune.init(-0.0078+30.1,0,0,-0.003+1.15, 5.1*pow(10,-5));
}

void derivatives(Body &P, Body &ext1, Body &ext2, Body &ext3, Body &ext4, Body &ext5, Body &ext6, Body &ext7, Body &ext8, double *k, double *dy) {
	//P is the object we are working on
	// ext1 and ext2 are the other bodies exerting force on planet
	dy[0] = P.val(2) + k[2];
	dy[1] = P.val(3) + k[3];
	dy[2] = -4.*ext1.mass()*pow(PI,2)*(P.val(0) - ext1.val(0) + k[0])/pow(pow(P.val(0) - ext1.val(0) + k[0],2) + pow(P.val(1) - ext1.val(1) + k[1],2),(3./2))
			-4.*ext2.mass()*pow(PI,2)*(P.val(0) - ext2.val(0) + k[0])/pow(pow(P.val(0) - ext2.val(0) + k[0],2) + pow(P.val(1) - ext3.val(1) + k[1],2),(3./2))
			-4.*ext3.mass()*pow(PI,2)*(P.val(0) - ext3.val(0) + k[0])/pow(pow(P.val(0) - ext3.val(0) + k[0],2) + pow(P.val(1) - ext3.val(1) + k[1],2),(3./2))
			-4.*ext4.mass()*pow(PI,2)*(P.val(0) - ext4.val(0) + k[0])/pow(pow(P.val(0) - ext4.val(0) + k[0],2) + pow(P.val(1) - ext4.val(1) + k[1],2),(3./2))
			-4.*ext5.mass()*pow(PI,2)*(P.val(0) - ext5.val(0) + k[0])/pow(pow(P.val(0) - ext5.val(0) + k[0],2) + pow(P.val(1) - ext5.val(1) + k[1],2),(3./2))
			-4.*ext6.mass()*pow(PI,2)*(P.val(0) - ext6.val(0) + k[0])/pow(pow(P.val(0) - ext6.val(0) + k[0],2) + pow(P.val(1) - ext6.val(1) + k[1],2),(3./2))
			-4.*ext7.mass()*pow(PI,2)*(P.val(0) - ext7.val(0) + k[0])/pow(pow(P.val(0) - ext7.val(0) + k[0],2) + pow(P.val(1) - ext7.val(1) + k[1],2),(3./2))
			-4.*ext8.mass()*pow(PI,2)*(P.val(0) - ext8.val(0) + k[0])/pow(pow(P.val(0) - ext8.val(0) + k[0],2) + pow(P.val(1) - ext8.val(1) + k[1],2),(3./2));

	dy[3] = -4.*ext1.mass()*pow(PI,2)*(P.val(1) - ext1.val(1) + k[0])/pow(pow(P.val(0) - ext1.val(0) + k[0],2) + pow(P.val(1) - ext1.val(1) + k[1],2),(3./2))
			-4.*ext2.mass()*pow(PI,2)*(P.val(1) - ext2.val(1) + k[0])/pow(pow(P.val(0) - ext2.val(0) + k[0],2) + pow(P.val(1) - ext2.val(1) + k[1],2),(3./2))
			-4.*ext3.mass()*pow(PI,2)*(P.val(1) - ext3.val(1) + k[0])/pow(pow(P.val(0) - ext3.val(0) + k[0],2) + pow(P.val(1) - ext3.val(1) + k[1],2),(3./2))
			-4.*ext4.mass()*pow(PI,2)*(P.val(1) - ext4.val(1) + k[0])/pow(pow(P.val(0) - ext4.val(0) + k[0],2) + pow(P.val(1) - ext4.val(1) + k[1],2),(3./2))
			-4.*ext5.mass()*pow(PI,2)*(P.val(1) - ext5.val(1) + k[0])/pow(pow(P.val(0) - ext5.val(0) + k[0],2) + pow(P.val(1) - ext5.val(1) + k[1],2),(3./2))
			-4.*ext6.mass()*pow(PI,2)*(P.val(1) - ext6.val(1) + k[0])/pow(pow(P.val(0) - ext6.val(0) + k[0],2) + pow(P.val(1) - ext6.val(1) + k[1],2),(3./2))
			-4.*ext7.mass()*pow(PI,2)*(P.val(1) - ext7.val(1) + k[0])/pow(pow(P.val(0) - ext7.val(0) + k[0],2) + pow(P.val(1) - ext7.val(1) + k[1],2),(3./2))
			-4.*ext8.mass()*pow(PI,2)*(P.val(1) - ext8.val(1) + k[0])/pow(pow(P.val(0) - ext8.val(0) + k[0],2) + pow(P.val(1) - ext8.val(1) + k[1],2),(3./2));
}

//Runge-Kutta function
void runge_kutta_4(Body &Planet, Body &ext1, Body &ext2, Body &ext3, Body &ext4, Body &ext5, Body &ext6, Body &ext7, Body &ext8, double h, void (*derivs)(Body &, Body &, Body&,Body &, Body &, Body&,Body &, Body &, Body&, double*, double *))
{
  int i; double newstate;
  double h2,h6; 
  double *k1, *k2, *k3, *k4, *f;
  //   allocate space for local vectors 
  k1 = new double [4];
  k2 =  new double [4];
  k3 =  new double [4];
  k4 = new double [4];
  f = new double [4]; //stores temporary f(y+k1/2) etc
  h6 = h/6.;
  h2 = .5*h;
  //get dy and find k1
  for (i = 0; i < 4; i++) { // k1
	//k1 is h*k1/2 for easier use of derivs
	k1[i] = h2*Planet.dval(i);
  }
  (*derivs)(Planet, ext1, ext2, ext3, ext4, ext5, ext6, ext7, ext8, k1, f);     // k2   
  for (i = 0; i < 4; i++) {//k2
    k2[i] = h2*f[i];//k2 is k2/2 for easier use of derivs
  }
  (*derivs)(Planet, ext1, ext2, ext3, ext4, ext5, ext6, ext7, ext8, k2, f); //  k3 
  for (i=0; i < 4; i++) {//k3
    k3[i] = h*f[i];//k3 is normal 
  }
  (*derivs)(Planet, ext1, ext2, ext3, ext4, ext5, ext6, ext7, ext8, k3, f);    // k4 
  for (i=0; i < 4; i++) {//k4
	k4[i] = h*f[i];  //k4 is normal
  }
  //      now we upgrade state of Body  
  for (i = 0; i < 4; i++){
	newstate = Planet.val(i)+1./6.*(2.*k1[i]+4.*k2[i]+2.*k3[i] + 1.*k4[i]);
    Planet.set(i,newstate);
  }
  delete []k1;
  delete [] k2;
  delete [] k3;
  delete [] k4;
  delete [] f;
} 


//main below
int main(int argc, char* argv[]) {

	//declare variables
	Body Sun;
	Body Mercury;
	Body Venus;
	Body Earth;
	Body Mars;
	Body Jupiter;
	Body Saturn;
	Body Uranus;
	Body Neptune;
	double t, h, tmax; 
	int steps;

	//open output file
	ofstream outfile;
 	outfile.open("9body.dat");
	
	//let user define timescale
	cout << "Number of steps? " << endl;
	cin >> steps;
	cout << "Total time? " << endl;
	cin >> tmax;
	initialize(Sun, Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune);
	h = 1.*tmax/steps;
	t = 0.;

	// loop calculations until assigned time-range has been reached
    while (t <= tmax){
		 //find initial derivatives due to all states
		Sun.move(Mercury);
		Sun.superpose(Venus);
		Sun.superpose(Earth);
		Sun.superpose(Mars);
		Sun.superpose(Jupiter);
		Sun.superpose(Saturn);
		Sun.superpose(Uranus);
		Sun.superpose(Neptune);

		Mercury.move(Sun);
		Mercury.superpose(Venus);
		Mercury.superpose(Earth);
		Mercury.superpose(Mars);
		Mercury.superpose(Jupiter);
		Mercury.superpose(Saturn);
		Mercury.superpose(Uranus);
		Mercury.superpose(Neptune);

		Venus.move(Sun);
		Venus.superpose(Mercury);
		Venus.superpose(Earth);
		Venus.superpose(Mars);
		Venus.superpose(Jupiter);
		Venus.superpose(Saturn);
		Venus.superpose(Uranus);
		Venus.superpose(Neptune);

		Earth.move(Sun);
		Earth.superpose(Mercury);
		Earth.superpose(Venus);
		Earth.superpose(Mars);
		Earth.superpose(Jupiter);
		Earth.superpose(Saturn);
		Earth.superpose(Uranus);
		Earth.superpose(Neptune);

		Mars.move(Sun);
		Mars.superpose(Mercury);
		Mars.superpose(Venus);
		Mars.superpose(Earth);
		Mars.superpose(Jupiter);
		Mars.superpose(Saturn);
		Mars.superpose(Uranus);
		Mars.superpose(Neptune);

		Jupiter.move(Sun);
		Jupiter.superpose(Mercury);
		Jupiter.superpose(Venus);
		Jupiter.superpose(Earth);
		Jupiter.superpose(Mars);
		Jupiter.superpose(Saturn);
		Jupiter.superpose(Uranus);
		Jupiter.superpose(Neptune);

		Saturn.move(Sun);
		Saturn.superpose(Mercury);
		Saturn.superpose(Venus);
		Saturn.superpose(Earth);
		Saturn.superpose(Mars);
		Saturn.superpose(Jupiter);
		Saturn.superpose(Uranus);
		Saturn.superpose(Neptune);

		Uranus.move(Sun);
		Uranus.superpose(Mercury);
		Uranus.superpose(Venus);
		Uranus.superpose(Earth);
		Uranus.superpose(Mars);
		Uranus.superpose(Jupiter);
		Uranus.superpose(Saturn);
		Uranus.superpose(Neptune);

		Neptune.move(Sun);
		Neptune.superpose(Mercury);
		Neptune.superpose(Venus);
		Neptune.superpose(Earth);
		Neptune.superpose(Mars);
		Neptune.superpose(Jupiter);
		Neptune.superpose(Saturn);
		Neptune.superpose(Uranus);

		//perform rk4 one step, increment one timestep, and save state to outfile  
		runge_kutta_4(Sun, Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, h, derivatives);	     
 		runge_kutta_4(Mercury, Sun, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, h, derivatives);	
		runge_kutta_4(Venus, Mercury, Sun, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, h, derivatives);	     
 		runge_kutta_4(Mars, Sun, Venus, Earth, Mercury, Jupiter, Saturn, Uranus, Neptune, h, derivatives);
		runge_kutta_4(Jupiter, Mercury, Venus, Earth, Mars, Sun, Saturn, Uranus, Neptune, h, derivatives);	     
 		runge_kutta_4(Saturn, Sun, Venus, Earth, Mars, Jupiter, Mercury, Uranus, Neptune, h, derivatives);
		runge_kutta_4(Uranus, Mercury, Venus, Earth, Mars, Jupiter, Saturn, Sun, Neptune, h, derivatives);	     
 		runge_kutta_4(Neptune, Sun, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Mercury, h, derivatives);
		
    	t += h;
		outfile << Sun.val(0) << endl;
		outfile << Sun.val(1) << endl;	
		outfile << Mercury.val(0) << endl;
		outfile << Mercury.val(1) << endl;
		outfile << Venus.val(0) << endl;
		outfile << Venus.val(1) << endl;
		outfile << Earth.val(0) << endl;
		outfile << Earth.val(1) << endl;	
		outfile << Mars.val(0) << endl;
		outfile << Mars.val(1) << endl;
		outfile << Jupiter.val(0) << endl;
		outfile << Jupiter.val(1) << endl;
		outfile << Saturn.val(0) << endl;
		outfile << Saturn.val(1) << endl;	
		outfile << Uranus.val(0) << endl;
		outfile << Uranus.val(1) << endl;
		outfile << Neptune.val(0) << endl;
		outfile << Neptune.val(1) << endl;
  	}
    outfile.close();  // close output file

	return 0;
}


























