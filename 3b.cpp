// first a version without classes
/*
This program solves for the equations of motion of earth as a small mass in 
a central field with the sun at the center of the field. Problem solved as 4 coupled differential equations
in Cartesian coordinates. 

What we are interested in is the path of the planet, i.e. the set of x and y progressed by the time.  

x'' = GM/((x^2+y^2)^(3/2))*x
y'' = GM/((x^2+y^2)^(3/2))*y

We define: 
dx/dt = u		du/dt = (4pi^2)/((x^2+y^2)^(3/2))*x
dy/dt = v		dv/dt = (4pi^2)/((x^2+y^2)^(3/2))*y

operating in AU, years.

A two-dimensional array represents x,y,u,v as functions of t:
y[0] == x
y[1] == y
y[2] == u
y[3] == v
dy[0]/dt = u
dy[1]/dt = v
dy[2]/dt = (4pi^2)/((x^2+y^2)^(3/2))*x
dy[3]/dt = (4pi^2)/((x^2+y^2)^(3/2))*y

For this program, we will downscale

The derivatives are calculated using Runge-Kutta 4 algorithm, and updated according to time-step h 
within a set of times t.

Specify initial velocity, initial position and timesteps and time interval.

*/


//outfile capabilities must be added...

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

//global
double PI = atan(1.0f) * 4.0f; // the value of pi 


//initialise function
void initialise (double* x_0, double* y_0, double* u_0, double* v_0, int* steps, double*tmax) {
	cout << "Initial x? " << endl;
	cin >> *x_0;
	cout << "Initial y? " << endl;
	cin >> *y_0;
	cout << "Initial dx/dt? " << endl;
	cin >> *u_0;
	cout << "Initial dy/dt? " << endl;
	cin >> *v_0;
	cout << "Number of steps? " << endl;
	cin >> *steps;
	cout << "Total time? " << endl;
	cin >> *tmax;
}
//initialise derivatives
//updates f for each variable based on present y and input in for pushing
//k-values 

void derivatives(double *k, double *y, double *dy) { 
	dy[0] = y[2] + k[2]; //x'
	dy[1] = y[3] + k[3]; //y'
	dy[2] = -4.*pow(PI,2)*(y[0] + k[0])/pow(pow((y[0]+ k[0]),2)+pow((y[1]+ k[1]),2),(3./2)); //u'
	dy[3] = -4.*pow(PI,2)*(y[1] + k[1])/pow(pow((y[0]+k[0]),2)+pow((y[1]+k[1]),2),(3./2)); //v'
} 
// t is only implicit

//Runge-Kutta function
//Need to check that arrays are not being destroyed
void runge_kutta_4(double *y, double *dy, double x, double h, 
                   double *yout, void (*derivs)(double*, double *, double *))
{
  int i;
  double      h2,h6; 
  double *k1, *k2, *k3, *k4, *zeero, *f;
  //   allocate space for local vectors  
  k1 = new double [4];
  k2 =  new double [4];
  k3 =  new double [4];
  k4 = new double [4];
  f = new double [4]; //stores temporary f(y+k1/2) etc
  h6 = h/6.;
  h2 = .5*h;
  for (i = 0; i < 4; i++) { // k1
    k1[i] = h2*y[i]; //k1 is h*k1/2 for easier use of derivs
  }
  (*derivs)(k1, y, f);     // k2   
  for (i = 0; i < 4; i++) {//k2
    k2[i] = h2*f[i];//k2 is k2/2 for easier use of derivs
  }
  (*derivs)(k2,y,f); //  k3 
  for (i=0; i < 4; i++) {//k3
    k3[i] = h*f[i];//k3 is normal 
  }
  (*derivs)(k3,y,f);    // k4 
  for (i=0; i < 4; i++) {//k4
	k4[i] = h*f[i];  //k4 is normal
  }
  //      now we upgrade y in the array yout  
  for (i = 0; i < 4; i++){
    yout[i] = y[i]+1./6.*(2.*k1[i]+4.*k2[i]+2.*k3[i] + 1.*k4[i]);
  }
  delete [] k1;
  delete [] k2;
  delete [] k3;
  delete [] k4;
  delete [] f;
} 


int main(int argc, char* argv[]) {
	//declare variables

	double t, h, tmax, E; 
	//add E0, M0, if needed, since energy and angular momentum conserved
	double *y, *dy, *yout, *zero;
	//in general we want to write out x,y,u,v,t and perhaps E and M
	// as a test, make yout write out to terminal first
	double x_0, y_0, u_0, v_0; //initial position and velocity
	int i, steps;
	ofstream outfile;
 	outfile.open("3bdata.dat");
	y = new double[4];
	dy = new double[4];
	yout = new double[4];
	initialise(&x_0, &y_0, &u_0, &v_0, &steps, &tmax); //let user define initial state

	h = 1.*tmax/steps;
	y[0] = x_0;
	y[1] = y_0;
	y[2] = u_0;
	y[3] = v_0;

	dy[0] = 0;
	dy[1] = 0;
	dy[2] = 0;
	dy[3] = 0;
	t = 0.;
	//testing below
	zero = new double[4];
	*zero = (0.,0.,0.,0.);
	//cout << y[0] << y[1] << y[2] << y[3] << endl;
	//cout << dy[0] << dy[1] << dy[2] << dy[3] << endl;

	//E0, M0 def if we need conservation equations
	// can use E0 to check and M0 to find the circle
    while (t <= tmax){
		 //find initial derivatives
   		 derivatives(zero, y, dy);    
		//perform rk4 one step       
   		 runge_kutta_4(y, dy, t, h, yout, derivatives); 
		//move results from rk4 to new state and print 
   		for (i = 0; i < 4; i++) {
  	  		y[i] = yout[i]; 
			//E = .5*(pow(y[2],2) + pow(y[3],2)) - 1./(pow(pow(y[0],2) + pow(y[1],2),1./2));
			//cout << yout[i] << endl; 
   		}
	//cout << E << endl;
	//cout << endl;	
	//increment time
    t += h;
	outfile << y[0] << endl;
	outfile << y[1] << endl;
  	}
	delete [] y; delete [] dy; delete [] yout;
    outfile.close();  // close output file

	return 0;
}



























