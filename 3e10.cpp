/*
Mass of Jupiter 10 times original. 
*/

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

//global constants
double PI = atan(1.0f) * 4.0f; // the value of pi 

//create class for planets and star. Body short for planetary body...
class Body {
	public:
	double x, y, u, v;
	double distx, disty; //distance from Jupiter, Xe - Xj, Ye - Yj
	double dx, dy, du, dv;
	void init(double, double, double, double);
	void set(int, double);
	void setd(int, double);
	void setdist(int, double, double);
	double getdist(int);
	double val(int);
	double dval(int);
	void move();
	void show(); //for testing
};
void Body::init(double a, double b, double c, double d) {
	x = a;
	y = b;
	u = c;
	v = d;
	dx = 0.;
	dy = 0.;
	du = 0.;
	dv = 0.;
}
void Body::set(int i, double a) {
	switch (i) {
		case 0:
			x = a;
			break;
		case 1:
			y = a;
			break;
		case 2:
			u = a;
			break;
		case 3:
			v = a;
			break;
	}
}

void Body::setd(int i, double a) {
	switch (i) {
		case 0:
			dx = a;
			break;
		case 1:
			dy = a;
			break;
		case 2:
			du = a;
			break;
		case 3:
			dv = a;
			break;
	}
}

double Body::val(int i) {
	switch(i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return u;
		case 3:
			return v;
	}
}
double Body::dval(int i) {
	switch(i) {
		case 0:
			return dx;
		case 1:
			return dy;
		case 2:
			return du;
		case 3:
			return dv;
	}	
}
void Body::setdist(int i, double E, double J) {
	switch(i) {
		case 0:
			distx = E - J;
			break;
		case 1:
			disty = E- J;
			break;
	}
}

void Body::show() {
	cout << x << endl;
	cout << y << endl;
	cout << u << endl;
	cout << v << endl;
}
void Body::move() {
	dx = u;
	dy = v;
	du = -4.*pow(PI,2)*(x)/pow(pow(x,2)+pow(y,2),(3./2));
	dv = -4.*pow(PI,2)*(y)/pow(pow(x,2)+pow(y,2),(3./2));
}

double Body::getdist(int i) {
	switch (i) {
		case 0:
			return distx;
		case 1: 
			return disty;
	}
}

//initialise function
void initialise (Body &Planet) {
	double a,b,c,d;
	cout << "Initial x? " << endl;
	cin >> a;
	cout << "Initial y? " << endl;
	cin >> b;
	cout << "Initial dx/dt? " << endl;
	cin >> c;
	cout << "Initial dy/dt? " << endl;
	cin >> d;
	Planet.init(a,b,c,d);
}


//updates f for each variable based on present y and input in for pushing
//k-values 
//this is now only for Jupiter
void derivatives(Body &Planet, double *k, double *y, double *dy) { 
	dy[0] = y[2] + k[2]; //x'
	dy[1] = y[3] + k[3]; //y'
	dy[2] = -4.*pow(PI,2)*(y[0] + k[0])/pow(pow((y[0]+ k[0]),2)+pow((y[1]+ k[1]),2),(3./2)); //u'
	dy[3] = -4.*pow(PI,2)*(y[1] + k[1])/pow(pow((y[0]+k[0]),2)+pow((y[1]+k[1]),2),(3./2)); //v'
} 
// t is only implicit

//this is for Earth
void derivativesE(Body &Planet, double *k, double *y, double *dy) {
	dy[0] = y[2] + k[2]; //x'
	dy[1] = y[3] + k[3]; //y'
	dy[2] = -4.*pow(PI,2)*(y[0] + k[0])/pow(pow((y[0]+ k[0]),2)+pow((y[1]+ k[1]),2),(3./2)); //u' sun only
	dy[3] = -4.*pow(PI,2)*(y[1] + k[1])/pow(pow((y[0]+k[0]),2)+pow((y[1]+k[1]),2),(3./2)); //v' sun only
	//u' with J force superposed
	//v' with J force superposed
	dy[2] = dy[2] - 4.*pow(PI,2)*pow(10.,-2)*(Planet.getdist(0)+k[0])/(pow(pow(Planet.getdist(0)+k[0],2)+pow(Planet.getdist(1)+k[1],2),3./2)); 
	dy[2] = dy[2] - 4.*pow(PI,2)*pow(10.,-2)*(Planet.getdist(1)+k[1])/(pow(pow(Planet.getdist(0)+k[0],2)+pow(Planet.getdist(1)+k[1],2),3./2)); 
}

//Runge-Kutta function
void runge_kutta_4(Body &Planet, double t, double h, void (*derivs)(Body &Planet, double*, double *, double *))
{
  int i; double newstate;
  double h2,h6; 
  double *k1, *k2, *k3, *k4, *f, *y;
  //   allocate space for local vectors 
  k1 = new double [4];
  k2 =  new double [4];
  k3 =  new double [4];
  k4 = new double [4];
  f = new double [4]; //stores temporary f(y+k1/2) etc
  y = new double [4]; //stores dy to make deriv able to handle them 
  h6 = h/6.;
  h2 = .5*h;
  //get dy and find k1
  for (i = 0; i < 4; i++) { // k1
	//k1 is h*k1/2 for easier use of derivs
	k1[i] = h2*Planet.dval(i);
	y[i] = Planet.val(i);
  }
  (*derivs)(Planet, k1, y, f);     // k2   
  for (i = 0; i < 4; i++) {//k2
    k2[i] = h2*f[i];//k2 is k2/2 for easier use of derivs
  }
  (*derivs)(Planet, k2,y,f); //  k3 
  for (i=0; i < 4; i++) {//k3
    k3[i] = h*f[i];//k3 is normal 
  }
  (*derivs)(Planet, k3,y,f);    // k4 
  for (i=0; i < 4; i++) {//k4
	k4[i] = h*f[i];  //k4 is normal
  }
  //      now we upgrade state of Body  
  for (i = 0; i < 4; i++){
	newstate = Planet.val(i)+1./6.*(2.*k1[i]+4.*k2[i]+2.*k3[i] + 1.*k4[i]);
    Planet.set(i,newstate);
  }
  delete []k1;
  delete [] k2;
  delete [] k3;
  delete [] k4;
  delete [] f;
  delete [] y;
} 


//main below
int main(int argc, char* argv[]) {
	//declare variables
	Body Earth;
	Body Jupiter;
	double t, h, tmax, E, M, utemp, vtemp; 
	double *yout, *zero;
	int i, steps;
	ofstream outfile;
 	outfile.open("3e10.dat");
	yout = new double[4];
	//timescale
	cout << "Number of steps? " << endl;
	cin >> steps;
	cout << "Total time? " << endl;
	cin >> tmax;
	cout << "Initial Data for Earth" << endl;
	initialise(Earth); //let user define initial states
	cout << "Initial Data for Jupiter" << endl;
	initialise(Jupiter);//define initial state of Jupiter
	h = 1.*tmax/steps;
	t = 0.;


	// can use E0 and M0 to check and prove stability
    while (t <= tmax){
		//find distance between Earth and Jupiter
		Earth.setdist(0,Earth.val(0), Jupiter.val(0));
		Earth.setdist(1,Earth.val(1), Jupiter.val(1));
		 //find initial derivatives
   		Jupiter.move();    
		Earth.move();
		//earth is then moved again to superpose force from Jupiter
		utemp = Earth.dval(2) - 4.*pow(PI,2)*pow(10.,-2)*(Earth.getdist(0))/(pow(pow(Earth.getdist(0),2)+pow(Earth.getdist(1),2),3./2)); 
		vtemp = Earth.dval(3) - 4.*pow(PI,2)*pow(10.,-2)*(Earth.getdist(1))/(pow(pow(Earth.getdist(0),2)+pow(Earth.getdist(1),2),3./2)); 
		Earth.setd(2,utemp);
		Earth.setd(3,vtemp);
		//perform rk4 one step and save state to outfile       
   		runge_kutta_4(Jupiter, t, h, derivatives); 	
		runge_kutta_4(Earth, t, h, derivativesE);	
   		//for (i = 0; i < 4; i++) {
			//E = .5*(pow(y[2],2) + pow(y[3],2)) - 1./(pow(pow(y[0],2) + pow(y[1],2),1./2));
			//M = y[0]*y[3] - y[1]*y[2];
   		//}
		//increment time
    	t += h;
		outfile << Jupiter.val(0) << endl;
		outfile << Jupiter.val(1) << endl;	
		outfile << Earth.val(0) << endl;
		outfile << Earth.val(1) << endl;
  	}
    outfile.close();  // close output file

	return 0;
}


























