#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

class Person {
	public:	
	int age;
	int notage;
	void set (int,int);
	void set_x (int,int);
	int find (int);
	int twice() {return (2*age);}
};
void Person::set(int a,int b) {
	age = b;
	notage = a;
}
void Person::set_x(int i, int num) {
	switch (i) {
		case 0: 
			notage = num;
			break;
		case 1:
			age = num;
			break;
	}
}
int Person::find(int i) {
	switch (i) {
		case 0: 
			return notage;
		case 1:
			return age;
	}
}
void TwiceAge(Person pers) {
	cout << pers.twice() << endl;
}

int main(int argc, char* argv[]) {
	Person A;
	A.set(5,24);
	cout << A.find(0)<< endl;
	return 0;
}

